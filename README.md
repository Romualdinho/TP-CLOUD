# ZOO MANAGER (RESTFUL API)

**Zoo Manager** is an Restful API used to manage a zoo.

## Features

### Expected Features
|**METHOD**|**URL**|**BODY**|**DESCRIPTION**|
| ------------ | ------------ | ------------ | ------------ |
|**GET**|/animals| |Returns all of the animals of the center|
|**POST**|/animals|**Animal**|Adds an animal to your center|
|**GET**|/animals/{animal_id}|   |Returns the animal identified by {animal_id}|
|**PUT**|/animals|**Animal**|Edits all animals|
|**DELETE**|/animals|   |Deletes all animals|
|**POST**|/animals/{animal_id}|**Animal**|Creates the animal identified by {animal_id}|
|**PUT**|/animals/{animal_id}|**Animal**|Edits the animal identified by {animal_id}|
|**DELETE**|/animals/{animal_id}|   |Deletes the animal identified by {animal_id}|
|**GET**|/find/byName/{animal_name}|   |Searches for an animal by name|
|**GET**|/find/at/{position}|   |Searches for an animal by position|
|**GET**|/find/near/{position}|   |Searches animals near a position (within a radius of 100 km)|
|**GET**|/animals/{animal_id}/wolf|   |Retrieves informations on the animal identified by {animal_id} using the [WolframAlpha][1] service|
|**GET**|/center/journey/from/{route}|   |Retrieves route information from a GPS location to your center using the [GraphHopper][2] service|

### Additional Features

|**METHOD**|**URL**|**BODY**|**DESCRIPTION**|
| ------------ | ------------ | ------------ | ------------ |
|**POST**|/cages|**Cage**|Adds a cage to your center|
|**GET**|/cages/{cage_name}|   |Returns the cage named {cage_name}|
|**PUT**|/cages|**Cage**|Edits all cages|
|**DELETE**|/cages|   |Deletes all cages|
|**PUT**|/cages/{cage_name}|**Cage**|Edits the cage named {cage_name}|
|**DELETE**|/cages/{cage_name}|   |Deletes the cage named {cage_name}|

### Attributes Format

- **animal_id** : `^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$`
- **position** : `^lat=\d*.\d*&lng=\d*.\d*$`
- **route** : `^lat=\d*.\d*&lng=\d*.\d*(&vehicle=(car|small_truck|truck|scooter|foot|hike|bike|mtb|racingbike))?$`
- **animal_name** : `^.*$`
- **cage_name** : `^.*$`

### Examples

- **Get all of the animals of the center :**
```
https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/animals
```

- **Get the animal identified by a specific UUID :**
```
https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/animals/41bcfc5c-49ea-453b-a8c9-e104a0343e11
```

- **Search for an animal named Tic :**
```
https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/find/byName/Tic
```

- **Search for an animal at Rouen :**
```
https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/find/at/lat=49.443889&lng=1.103333
```

- **Search animals near Rouen (within a radius of 100 km) :**
```
https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/find/near/lat=49.443889&lng=1.103333
```

- **Get informations about an animal identified by a specific UUID :**
```
https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/animals/a0264613-bfe9-46ca-bb33-70f4fd3d637d/wolf
```

- **Get route information from Rouen to the center (car by default) :**
```
https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/center/journey/from/lat=49.443889&lng=1.103333
```

- **Get route information from Rouen to the center using a bike :**
```
https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/center/journey/from/lat=49.443889&lng=1.103333&vehicle=bike
```

- **Get the cage named Rouen :**
```
https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager/cages/Rouen
```

## Setting up

1. Put the credentials of your database in [**persistence.xml**](/ZOO_MANAGER/src/main/resources/META-INF/persistence.xml) (set "*create*" at "*hibernate.hbm2ddl.auto*" for auto-creates the schema).
1. Build the WAR file of the application.
1. Deploy the WAR on your server (Tomcat by example).

**This service is currently hosted on [BlueMix](https://zoo-manager-rest.eu-gb.mybluemix.net/zoo-manager)**.

## Built With

- [Java 8](https://www.java.com) &mdash; A computer programming language
- [Apache CXF](http://cxf.apache.org/) &mdash; An open source services framework
- [Spring](https://spring.io/) &mdash; An application framework and inversion of control container for the Java platform
- [JAX-RS](https://en.wikipedia.org/wiki/Java_API_for_RESTful_Web_Services) &mdash; A Java programming language API for creating REST web services
- [Hibernate](http://hibernate.org/) &mdash; An object-relational mapping tool for the Java programming language
- [MySQL](https://www.mysql.com/) &mdash; An open-source relational database management system (RDBMS)
- [Wolfram|Alpha](https://www.wolframalpha.com/) &mdash; A computational knowledge engine API
- [GraphHopper](https://graphhopper.com) &mdash; A fast Directions API with world wide data from OpenStreetMap and route optimization


## Contributors

The original contributors can be found in [**AUTHORS.MD**](/AUTHORS.md).


[1]: https://products.wolframalpha.com/api/documentation/ "Wolfram|Alpha"
[2]: https://graphhopper.com/api/1/docs/routing/ "GraphHopper"
