package tp.dao;

import tp.model.Cage;

public interface CageDAO {
    void create(Cage cage) throws DAOException;
    void updateByName(Cage cage, String name) throws DAOException;
    void updateAll(Cage cage) throws DAOException;
    void deleteByName(String name) throws DAOException;
    void deleteAll() throws DAOException;
    Cage findById(Long id) throws DAOException;
    Cage findByName(String name) throws DAOException;
}
