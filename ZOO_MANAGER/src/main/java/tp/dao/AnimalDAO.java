package tp.dao;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

import java.util.List;
import java.util.UUID;

public interface AnimalDAO {
    void create(Animal animal) throws DAOException;
    void createById(Animal animal, UUID id) throws DAOException;
    void updateById(Animal animal, UUID id) throws DAOException;
    void updateAll(Animal animal) throws DAOException;
    void deleteById(UUID id) throws DAOException;
    void deleteAll() throws DAOException;
    Animal findById(UUID id) throws DAOException;
    Animal findByName(String name) throws DAOException;
    Animal findAtPosition(Position position) throws DAOException;
    Cage findNearPosition(Position position) throws DAOException;
    Center findAll() throws DAOException;
}