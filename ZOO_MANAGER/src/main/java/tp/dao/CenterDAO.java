package tp.dao;

import tp.model.Center;

public interface CenterDAO {
    long DEFAULT_ID = 1;

    void create(Center center);
    void updateById(Center center, Long id) throws DAOException;
    void deleteById(Long id) throws DAOException;
    Center findById(Long id) throws DAOException;
}
