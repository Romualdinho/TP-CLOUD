package tp.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import tp.dao.AnimalDAO;
import org.springframework.stereotype.Repository;
import tp.dao.CageDAO;
import tp.dao.CenterDAO;
import tp.dao.DAOException;
import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

@Repository
public class AnimalDAOImpl implements AnimalDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CenterDAO centerDAO;

    @Autowired
    private CageDAO cageDAO;

    @Override
    public void create(Animal animal) throws DAOException {
        // Vérifie qu'un animal ayant le même identifiant n'existe pas déjà
        if (entityManager.find(Animal.class, animal.getId().toString()) != null) {
            throw new DAOException("The animal identified by " + animal.getId() + " already exists");
        }

        // Récupère la cage de destination de l'animal
        Cage cage = cageDAO.findByName(animal.getCage());

        // Vérifie que la cage peut accepter un nouvel animal
        if (cage.getResidents().size() >= cage.getCapacity()) {
            throw new DAOException("The maximum capacity of the cage " + cage.getName() + " is already reached");
        }

        // Ajoute l'animal dans la cage
        cage.getResidents().add(animal);
    }

    @Override
    public void createById(Animal animal, UUID id) throws DAOException {
        create(new Animal(animal.getName(), animal.getCage(), animal.getSpecies(), id));
    }

    @Override
    public void updateById(Animal animal, UUID id) throws DAOException {
        // Récupère la nouvelle cage
        Cage cage = cageDAO.findByName(animal.getCage());

        // Récupère l'animal devant être modifié
        Animal oldAnimal = findById(id);

        // Vérifie si l'animal doit changer de cage
        if (!oldAnimal.getCage().equals(animal.getCage())) {
            if (cage.getResidents().size() >= cage.getCapacity()) {
                throw new DAOException("The maximum capacity of the cage " + cage.getName() + " is already reached");
            }

            // Supprime l'animal
            deleteById(id);

            // Flush pour éviter les conflits avec la requête qui suit
            entityManager.flush();

            // Réajout de l'animal
            createById(animal, id);
        } else {
            // Mise a jour des propriétés de l'animal
            oldAnimal.setName(animal.getName());
            oldAnimal.setSpecies(animal.getSpecies());
        }
    }

    @Override
    public void updateAll(Animal animal) throws DAOException {
        // Vérifie si la cage existe
        Cage cage  = cageDAO.findByName(animal.getCage());

        // Récupère la liste de toutes les cages du centre
        Set<Cage> cages = centerDAO.findById(CenterDAO.DEFAULT_ID).getCages();

        // Sauvegarde tous les animaux
        Set<Animal> animals = new HashSet<Animal>();
        cages.forEach(c -> animals.addAll(c.getResidents()));

        // Vide toutes les cages
        cages.forEach(c -> c.getResidents().clear());

        // Flush pour éviter les conflits avec la requête suivante
        entityManager.flush();

        // Mise a jour des propriétés de l'animal
        animals.forEach(a -> {
            a.setName(animal.getName());
            a.setCage(animal.getCage());
            a.setSpecies(animal.getSpecies());
        });

        // Ajout de tous les animaux dans la nouvelle cage
        cage.getResidents().addAll(animals);
    }

    @Override
    public void deleteById(UUID id) throws DAOException {
        // Récupère l'animal devant être supprimé
        Animal animal = findById(id);

        // Supprime l'animal de sa nouvelle cage
        cageDAO.findByName(animal.getCage()).getResidents().remove(animal);
    }

    @Override
    public void deleteAll() throws DAOException {
        // Récupère le centre
        Center center = centerDAO.findById(CenterDAO.DEFAULT_ID);

        // Vide toutes les cages du centre
        for (Cage cage : center.getCages()) {
            cage.getResidents().clear();
        }
    }

    @Override
    public Animal findById(UUID id) throws DAOException {
        // Récupère l'animal
        Animal animal = entityManager.find(Animal.class, id.toString());

        // Lance une exception si l'animal n'est pas dans la base de données
        if (animal == null) {
            throw new DAOException("No animal found with ID : " + id);
        }

        return animal;
    }

    @Override
    public Animal findByName(String name) throws DAOException {
        // Récupère la liste des animaux ayant pour nom "name"
        Query query = entityManager.createQuery("select animal from Center center join center.cages cage join cage.residents animal where center.id = :center_id and animal.name = :name");
        query.setParameter("center_id", CenterDAO.DEFAULT_ID);
        query.setParameter("name", name);
        query.setMaxResults(1);
        List<Animal> animals = query.getResultList();

        // Retourne le premier animal trouvé
        return animals.stream()
                .findFirst()
                .orElseThrow(() -> new DAOException("No animal found with name : " + name));
    }

    @Override
    public Animal findAtPosition(Position position) throws DAOException {
        // Récupère la liste des cages ayant pour position exacte "position"
        Query query = entityManager.createQuery("select cage from Center center join center.cages cage where center.id = :center_id and cage.position.latitude = :lat and cage.position.longitude = :lgt");
        query.setParameter("center_id", CenterDAO.DEFAULT_ID);
        query.setParameter("lat", position.getLatitude());
        query.setParameter("lgt", position.getLongitude());
        query.setMaxResults(1);
        List<Cage> cages = query.getResultList();

        // Retourne la première cage trouvée
        return cages.stream()
                .map(Cage::getResidents)
                .flatMap(Collection::stream)
                .findFirst()
                .orElseThrow(() -> new DAOException("No animal found at the position : " + position));
    }

    @Override
    public Cage findNearPosition(Position position) throws DAOException {
        // Récupère l'ensemble des cages du centre
        Set<Cage> cages = centerDAO.findById(CenterDAO.DEFAULT_ID).getCages();

        // Retounre la première cage étant proche de la position
        return cages.stream()
                .filter(cage -> cage.getPosition().isNear(position))
                .findFirst()
                .orElseThrow(() -> new DAOException("No animals found near position : " + position));
    }

    @Override
    public Center findAll() throws DAOException {
        // Retourne le centre
        return centerDAO.findById(CenterDAO.DEFAULT_ID);
    }
}