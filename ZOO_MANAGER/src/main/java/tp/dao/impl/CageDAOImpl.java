package tp.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tp.dao.CageDAO;
import tp.dao.CenterDAO;
import tp.dao.DAOException;
import tp.model.Cage;
import tp.model.Center;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class CageDAOImpl implements CageDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CenterDAO centerDAO;

    @Override
    public void create(Cage cage) throws DAOException {
        // Vérifie si la capacité est supérieure à 0
        if (cage.getCapacity() <= 0) {
            throw new DAOException("Capacity of cage must be greater than 0");
        }

        // Vérifie que les résidents peuvent être accueilis dans la cage
        if (cage.getResidents() != null && cage.getResidents().size() > cage.getCapacity()) {
            throw new DAOException("The number of residents exceeds the capacity");
        }

        // Vérifie que la cage n'existe pas déjà
        try {
            findByName(cage.getName());
        } catch (DAOException e) {
            centerDAO.findById(CenterDAO.DEFAULT_ID).getCages().add(cage);
            return;
        }

        // Lance une exception si la cage existe déjà
        throw new DAOException("The cage " + cage.getName() + " already exists");
    }

    @Override
    public void updateByName(Cage cage, String name) throws DAOException {
        // Vérifie si la capacité est supérieure à 0
        if (cage.getCapacity() <= 0) {
            throw new DAOException("Capacity of cage must be greater than 0");
        }

        // Vérifie que les résidents peuvent être accueilis dans la cage
        if (cage.getResidents().size() > cage.getCapacity()) {
            throw new DAOException("The number of residents exceeds the capacity");
        }

        // Modifie les propriétés de la cage
        Cage oldCage = findByName(name);
        oldCage.setPosition(cage.getPosition());
        oldCage.setCapacity(cage.getCapacity());
    }

    @Override
    public void updateAll(Cage cage) throws DAOException {
        // Vérifie si la capacité est supérieure à 0
        if (cage.getCapacity() <= 0) {
            throw new DAOException("Capacity of cage must be greater than 0");
        }

        // Vérifie que les résidents peuvent être accueilis dans la cage
        if (cage.getResidents().size() > cage.getCapacity()) {
            throw new DAOException("The number of residents exceeds the capacity");
        }

        // Récupère le centre
        Center center = centerDAO.findById(CenterDAO.DEFAULT_ID);

        // Modifie les propriétés de l'ensemble des cages du centre
        for (Cage c : center.getCages()) {
            c.setPosition(cage.getPosition());
            c.setCapacity(cage.getCapacity());
        }
    }

    @Override
    public void deleteByName(String name) throws DAOException {
        // Supprime la cage ayant pour nom "name"
        Center center = centerDAO.findById(CenterDAO.DEFAULT_ID);
        center.getCages().remove(findByName(name));
    }

    @Override
    public void deleteAll() throws DAOException {
        // Supprime l'ensemble des cages
        centerDAO.findById(CenterDAO.DEFAULT_ID).getCages().clear();
    }

    @Override
    public Cage findById(Long id) throws DAOException {
        // Récupère la cage ayant pour identifiant "id"
        Cage cage = entityManager.find(Cage.class, id);

        // Lance une exception si la cage n'est pas dans la base de données
        if (cage == null) {
            throw new DAOException("No cage found with ID : " + id);
        }

        return cage;
    }

    @Override
    public Cage findByName(String name) throws DAOException {
        // Récupère la liste des cages ayant pour nom "name"
        Query query = entityManager.createQuery("select cage from Center center join center.cages cage where center.id = :id and cage.name = :name");
        query.setParameter("id", CenterDAO.DEFAULT_ID);
        query.setParameter("name", name);
        query.setMaxResults(1);
        List<Cage> cages = query.getResultList();

        // Retourne la première cage trouvée
        return cages.stream()
                .findFirst()
                .orElseThrow(() -> new DAOException("No cage found with name : " + name));
    }
}