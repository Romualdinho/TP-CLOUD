package tp.dao.impl;

import org.springframework.stereotype.Repository;
import tp.dao.CenterDAO;
import tp.dao.DAOException;
import tp.model.Cage;
import tp.model.Center;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CenterDAOImpl implements CenterDAO {
    @PersistenceContext
    private EntityManager entityManager;

    public void create(Center center) {
        // Sauvegarde le centre dans la base de données
        entityManager.persist(center);
    }

    @Override
    public void updateById(Center center, Long id) throws DAOException {
        // Modifie les propriétés du centre
        Center oldCenter = findById(id);
        oldCenter.setName(center.getName());
        oldCenter.setCages(center.getCages());
        oldCenter.setPosition(center.getPosition());
    }

    @Override
    public void deleteById(Long id) throws DAOException {
        // Le centre ayant pour ID "DEFAULT_ID" ne peut pas être supprimé
        if (id == DEFAULT_ID) {
            throw new DAOException("You can not delete the main center");
        }

        // Supprime le centre
        entityManager.remove(findById(id));
    }

    @Override
    public Center findById(Long id) throws DAOException {
        // Récupère le centre
        Center center = entityManager.find(Center.class, id);

        // Lance une exception si le centre n'est pas dans la base de données
        if (center == null) {
            throw new DAOException("No center found with ID : " + id);
        }

        return center;
    }
}