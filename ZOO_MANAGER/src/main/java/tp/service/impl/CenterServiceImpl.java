package tp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import tp.dao.CenterDAO;
import tp.dao.DAOException;
import tp.model.Position;
import tp.service.CenterService;
import tp.service.util.RestUtils;

import javax.transaction.Transactional;
import javax.xml.ws.http.HTTPException;
import java.awt.*;
import java.io.IOException;
import java.util.*;

public class CenterServiceImpl implements CenterService {
    @Autowired
    private CenterDAO dao;

    @Transactional
    @Override
    public String getRouteInformation(String route) throws DAOException {
        if (!RestUtils.ROUTE_PATTERN.matcher(route).matches()) {
            throw new HTTPException(404);
        }

        // Véhicule par défaut si l'utilisateur n'a rien spécifié
        String vehicle = "car";

        // Vérifie si l'utilisateur a spécifié un véhicule particulier pour l'élaboration du trajet
        if (route.contains("&vehicle=")) {
            // Récupération du véhicule désiré
            vehicle = route.substring(route.indexOf("&vehicle=") + 9);
        }

        // Récupère la position de départ du trajet
        Position sourcePosition = RestUtils.parsePosition(route.substring(0, Math.max(route.indexOf("&vehicle="), route.length())));

        // La position de destination du trajet correspond à l'adresse du centre
        Position targetPosition = dao.findById(CenterDAO.DEFAULT_ID).getPosition();

        // Forme l'URL en respectant l'API imposée par GraphHopper
        String url = String.format(Locale.US, "https://graphhopper.com/api/1/route?point=%f,%f&point=%f,%f&vehicle=%s&locale=fr&type=gpx&key=%s",
                sourcePosition.getLatitude(), sourcePosition.getLongitude(), targetPosition.getLatitude(),
                targetPosition.getLongitude(), vehicle, RestUtils.GRAPHHOPPER_API_KEY);

        System.out.println(url);

        try {
            return RestUtils.executeGetRequest(url);
        } catch (IOException e) {
            throw new HTTPException(404);
        }
    }
}
