package tp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import tp.dao.CageDAO;
import tp.dao.DAOException;
import tp.model.Cage;
import tp.service.CageService;

import javax.transaction.Transactional;

public class CageServiceImpl implements CageService {
    @Autowired
    private CageDAO dao;

    @Transactional
    @Override
    public Cage findByName(String name) throws DAOException {
        return dao.findByName(name);
    }

    @Transactional
    @Override
    public void create(Cage cage) throws DAOException {
        dao.create(cage);
    }

    @Transactional
    @Override
    public void updateByName(Cage cage, String name) throws DAOException {
        dao.updateByName(cage, name);
    }

    @Transactional
    @Override
    public void updateAll(Cage cage) throws DAOException {
        dao.updateAll(cage);
    }

    @Transactional
    @Override
    public void deleteByName(String name) throws DAOException {
        dao.deleteByName(name);
    }

    @Transactional
    @Override
    public void deleteAll() throws DAOException {
        dao.deleteAll();
    }
}
