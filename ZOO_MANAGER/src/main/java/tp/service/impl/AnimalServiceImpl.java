package tp.service.impl;

import tp.dao.AnimalDAO;
import tp.dao.DAOException;
import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import tp.service.util.RestUtils;

import javax.transaction.Transactional;
import javax.xml.ws.http.HTTPException;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.UUID;

public class AnimalServiceImpl implements AnimalService {

    @Autowired
    private AnimalDAO dao;

    @Transactional
    @Override
    public Animal findById(String id) throws DAOException {
        if (!RestUtils.UUID_PATTERN.matcher(id).matches()) {
            throw new HTTPException(404);
        }

        return dao.findById(UUID.fromString(id));
    }

    @Transactional
    @Override
    public Center findAll() throws DAOException {
        return dao.findAll();
    }

    @Transactional
    @Override
    public Animal findByName(String name) throws DAOException {
        return dao.findByName(name);
    }

    @Transactional
    @Override
    public Animal findAtPosition(String position) throws DAOException {
        if (!RestUtils.POSITION_PATTERN.matcher(position).matches()) {
            throw new HTTPException(404);
        }

        return dao.findAtPosition(RestUtils.parsePosition(position));
    }

    @Transactional
    @Override
    public Cage findNearPosition(String position) throws DAOException {
        if (!RestUtils.POSITION_PATTERN.matcher(position).matches()) {
            throw new HTTPException(404);
        }

        return dao.findNearPosition(RestUtils.parsePosition(position));
    }

    @Transactional
    @Override
    public String getInformation(String id) throws DAOException {
        if (!RestUtils.UUID_PATTERN.matcher(id).matches()) {
            throw new HTTPException(404);
        }

        // Récupération de l'animal dont on souhaite obtenir des informations
        Animal animal = dao.findById(UUID.fromString(id));

        try {
            // Encodage du paramètre de requête
            String species = URLEncoder.encode(animal.getSpecies(), "UTF-8");

            //Forme l'URL en respectant l'API imposée par WolframAlpha
            String url = String.format("http://api.wolframalpha.com/v2/query?appid=%s&input=%s", RestUtils.WOLFRAM_API_KEY, species);

            return RestUtils.executeGetRequest(url);
        } catch (IOException e) {
            throw new HTTPException(404);
        }
    }

    @Transactional
    @Override
    public void create(Animal animal) throws DAOException {
        dao.create(animal);
    }

    @Transactional
    @Override
    public void createById(Animal animal, String id) throws DAOException {
        if (!RestUtils.UUID_PATTERN.matcher(id).matches()) {
            throw new HTTPException(404);
        }

        dao.createById(animal, UUID.fromString(id));
    }

    @Transactional
    @Override
    public void updateById(Animal animal, String id) throws DAOException {
        if (!RestUtils.UUID_PATTERN.matcher(id).matches()) {
            throw new HTTPException(404);
        }

        dao.updateById(animal, UUID.fromString(id));
    }

    @Transactional
    @Override
    public void updateAll(Animal animal) throws DAOException {
        dao.updateAll(animal);
    }

    @Transactional
    @Override
    public void deleteById(String id) throws DAOException {
        if (!RestUtils.UUID_PATTERN.matcher(id).matches()) {
            throw new HTTPException(404);
        }

        dao.deleteById(UUID.fromString(id));
    }

    @Transactional
    @Override
    public void deleteAll() throws DAOException {
        dao.deleteAll();
    }
}