package tp.service;

import tp.dao.DAOException;
import tp.model.Cage;

import javax.ws.rs.*;

@Path("/zoo-manager/")
public interface CageService {
    /**
     * GET method bound to calls on /cages/{something}
     */
    @GET
    @Path("/cages/{cage_name}/")
    @Produces("application/xml")
    Cage findByName(@PathParam("cage_name") String name) throws DAOException;

    /**
     * POST method bound to calls on /cages
     */
    @POST
    @Path("/cages/")
    @Consumes({"application/xml", "application/json"})
    @Produces("application/xml")
    void create(Cage cage) throws DAOException;

    /**
     * PUT method bound to calls on /cages/{something}
     */
    @PUT
    @Path("/cages/{cage_name}")
    @Consumes({"application/xml", "application/json"})
    @Produces("application/xml")
    void updateByName(Cage cage, @PathParam("cage_name") String name) throws DAOException;

    /**
     * PUT method bound to calls on /cages
     */
    @PUT
    @Path("/cages/")
    @Consumes({"application/xml", "application/json"})
    @Produces("application/xml")
    void updateAll(Cage cage) throws DAOException;

    /**
     * DELETE method bound to calls on /cages/{something}
     */
    @DELETE
    @Path("/cages/{cage_name}")
    @Produces("application/xml")
    void deleteByName(@PathParam("cage_name") String name) throws DAOException;

    /**
     * DELETE method bound to calls on /cages
     */
    @DELETE
    @Path("/cages/")
    @Produces("application/xml")
    void deleteAll() throws DAOException;
}