package tp.service;

import tp.dao.DAOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/zoo-manager/")
public interface CenterService {
    /**
     * GET method bound to calls on /center/journey/from/{route}
     */
    @GET
    @Path("/center/journey/from/{route}")
    @Produces("application/xml")
    String getRouteInformation(@PathParam("route") String route) throws DAOException;
}
