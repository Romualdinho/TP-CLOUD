package tp.service;

import tp.dao.DAOException;
import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;

import javax.ws.rs.*;

@Path("/zoo-manager/")
public interface AnimalService {
    /**
     * GET method bound to calls on /animals/{something}
     */
    @GET
    @Path("/animals/{animal_id}/")
    @Produces("application/xml")
    Animal findById(@PathParam("animal_id") String id) throws DAOException;

    /**
     * GET method bound to calls on /animals
     */
    @GET
    @Path("/animals/")
    @Produces("application/xml")
    Center findAll() throws DAOException;

    /**
     * GET method bound to calls on /find/byName{something}
     */
    @GET
    @Path("/find/byName/{animal_name}")
    @Produces("application/xml")
    Animal findByName(@PathParam("animal_name") String name) throws DAOException;

    /**
     * GET method bound to calls on /find/at/{something}
     */
    @GET
    @Path("/find/at/{position}")
    @Produces("application/xml")
    Animal findAtPosition(@PathParam("position") String position) throws DAOException;

    /**
     * GET method bound to calls on /find/near/{something}
     */
    @GET
    @Path("/find/near/{position}")
    @Produces("application/xml")
    Cage findNearPosition(@PathParam("position") String position) throws DAOException;

    /**
     * GET method bound to calls on /animals/{something}/wolf
     */
    @GET
    @Path("/animals/{animal_id}/wolf")
    @Produces("application/xml")
    String getInformation(@PathParam("animal_id") String id) throws DAOException;

    /**
     * POST method bound to calls on /animals
     */
    @POST
    @Path("/animals/")
    @Consumes({"application/xml", "application/json"})
    void create(Animal animal) throws DAOException;

    /**
     * POST method bound to calls on /animals/{something}
     */
    @POST
    @Path("/animals/{animal_id}/")
    @Consumes({"application/xml", "application/json"})
    void createById(Animal animal, @PathParam("animal_id") String id) throws DAOException;

    /**
     * PUT method bound to calls on /animals/{something}
     */
    @PUT
    @Path("/animals/{animal_id}")
    @Consumes({"application/xml", "application/json"})
    void updateById(Animal animal, @PathParam("animal_id") String id) throws DAOException;

    /**
     * PUT method bound to calls on /animals
     */
    @PUT
    @Path("/animals/")
    @Consumes({"application/xml", "application/json"})
    void updateAll(Animal animal) throws DAOException;

    /**
     * DELETE method bound to calls on /animals/{something}
     */
    @DELETE
    @Path("/animals/{animal_id}")
    void deleteById(@PathParam("animal_id") String id) throws DAOException;

    /**
     * DELETE method bound to calls on /animals
     */
    @DELETE
    @Path("/animals/")
    void deleteAll() throws DAOException;
}
