package tp.service.util;

import tp.model.Position;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Pattern;

public class RestUtils {
    /**
     * API Keys
     */
    public static final String WOLFRAM_API_KEY = "WX2KQW-YEKXQYTTV4";
    public static final String GRAPHHOPPER_API_KEY = "25bf2c42-b773-4d10-8f32-c48af5a91a9e";

    /**
     * Patterns
     */
    public static final Pattern UUID_PATTERN = Pattern.compile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
    public static final Pattern POSITION_PATTERN = Pattern.compile("^lat=\\d*.\\d*&lng=\\d*.\\d*$");
    public static final Pattern ROUTE_PATTERN = Pattern.compile("^lat=\\d*.\\d*&lng=\\d*.\\d*(&vehicle=(car|small_truck|truck|scooter|foot|hike|bike|mtb|racingbike))?$");

    /**
     * Execute a request and return the result
     */
    public static String executeGetRequest(String url) throws IOException {
        // Effectue une requête HTTP à l'URL indiqué
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();

        // Methode de la requête
        connection.setRequestMethod("GET");

        // Récupération du résultat de la requête dans un BufferedReader
        BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

        // Lecture du résultat
        StringBuilder response = new StringBuilder();
        String currentLine;
        while ((currentLine = br.readLine()) != null) {
            response.append(currentLine);
        }

        br.close();

        // Retourne le résultat
        return String.valueOf(response);
    }

    /**
     * Parse String to Position
     */
    public static Position parsePosition(String value)  {
        String position_parts[] =  value.split("&");
        return new Position(Double.parseDouble(position_parts[0].replace("lat=", "")), Double.parseDouble(position_parts[1].replaceAll("lng=", "")));
    }
}