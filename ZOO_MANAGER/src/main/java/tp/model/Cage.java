package tp.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "tbl_cage")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Cage implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @XmlTransient
    private Long id;

    /**
     * The name of this cage
     */
    @Column(name = "name")
    private String name;
    /**
     * The visitor entrance location
     */
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
    @Cascade(org.hibernate.annotations.CascadeType.REMOVE)
    @JoinColumn(name = "position_id")
    private Position position;
    /**
     * The maximum number of animals in this cage
     */
    @Column(name = "capacity")
    private Integer capacity;
    /**
     * The animals in this cage.
     */
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
    @Cascade(org.hibernate.annotations.CascadeType.REMOVE)
    @JoinTable(name = "tbl_cage_animal",
            joinColumns = @JoinColumn(name = "cage_id"),
            inverseJoinColumns = @JoinColumn(name = "animal_id")
    )
    private Set<Animal> residents;

    public Cage() {
    }

    public Cage(String name, Position position, Integer capacity, Set<Animal> residents) {
        this.name = name;
        this.position = position;
        this.capacity = capacity;
        this.residents = residents;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Position getPosition() {
        return position;
    }

    public Set<Animal> getResidents() {
        return residents;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setResidents(Set<Animal> residents) {
        this.residents = residents;
    }
}
