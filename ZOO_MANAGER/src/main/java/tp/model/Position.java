package tp.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@Entity
@Table(name = "tbl_position")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Position implements Serializable {

    private static final int MAX_NEAR_DISTANCE = 100000; // 100 kilometers

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @XmlTransient
    private Long id;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    public Position(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Position() {
    }

    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof Position) {
            Position otherPosition = (Position) o;
            result = otherPosition.latitude == this.latitude &&
                    otherPosition.longitude == this.longitude;
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isNear(Position position) {
        float pk = (float) (180.f / Math.PI);

        double a1 = latitude / pk;
        double a2 = longitude / pk;
        double b1 = position.getLatitude() / pk;
        double b2 = position.getLongitude() / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return (6366000 * tt) <= MAX_NEAR_DISTANCE;
    }

    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("(").append(latitude).append(", ").append(longitude).append(")");
        return buffer.toString();
    }
}