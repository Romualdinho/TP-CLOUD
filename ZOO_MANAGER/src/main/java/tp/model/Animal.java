package tp.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "tbl_animal")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Animal implements Serializable {
    /**
     * Name of the animal
     */
    @Column(name = "name")
    private String name;

    /**
     * The place where it currently live in.
     */
    @Column(name = "cage")
    private String cage;
    /**
     * The animal's species
     */
    @Column(name = "species")
    private String species;
    /**
     * The animal unique identifier.
     */

    @Transient
    private UUID id;

    public Animal() {
    }

    public Animal(String name, String cage, String species, UUID id) {
        this.name = name;
        this.cage = cage;
        this.species = species;
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("<%s : %s ( %s ) is a %s currently in %s>", this.getClass().getName(), name, id, species, cage);
    }

    @Transient
    public UUID getId() {
        return id;
    }

    @Id
    @Column(name = "id")
    public String getIdAsString() {
        return id.toString();
    }

    public String getCage() {
        return cage;
    }

    public String getName() {
        return name;
    }

    public String getSpecies() {
        return species;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setIdAsString(String value) {
        id = UUID.fromString(value);
    }

    public void setCage(String cage) {
        this.cage = cage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSpecies(String species) {
        this.species = species;
    }
}
